# EdgeFiller2D

Algoritem za poljnjenje diskretno podanih krivulj z GSF algoritmom s pomočjo kubičnih zlepkov.

## EdgeFiller.hpp

Datoteka, ki vsebuje dejansko implementacijo algoritma. Sledi primer tipične uporabe. Naj bo `pts` bodisi `Range<Vec2d>` bodisi `DomainDiscretization<Vec2d>`, kjer je razdalja do najbližje točke enaka `h`. Če želimo napolniti te točke, da bo razdalja do najbližje npr. `h/10`, najprej ustvarimo nov objekt

```c++
EdgeFiller2D ef(pts);
```

nato pa mu damo novo (prazno) domeno, ki jo naj zapolni, in željeno 'gostoto'

```c++
ef(new_domain, h/10);
```

Tedaj bo `new_domain` ravno željena napoljnena domena.

## main.cpp

Zgled uporabe algoritma.

## utils.hpp

Datoteka ki vsebuje le funkcijo `matlab_dump`, ki zapiše točke iz danega `Range` objekta v datoteko, ki jo lahko naložimo v Matlabu.

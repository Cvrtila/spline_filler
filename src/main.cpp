#include <iostream>
#include <medusa/Medusa.hpp>
#include "EdgeFiller.hpp"

#include "utils.hpp"

using namespace mm;

int main() {
    // Usage example

    // First generate the points to be given to the algorithm
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r * cos(t(0)), r * sin(t(0)));
    };
    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r = (-1.5 * pow(abs(cos(1.5 * t(0))),
                                   sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
                        3 * pow(abs(cos(1.5 * t(0))),
                                sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0))
                        * log(abs(cos(1.5 * t(0))))) / cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));
        return jm;
    };
    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * PI});
    // Generate domain
    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);
    double h = 0.1;
    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    gsf.seed(987);
    gsf(domain, param_bs, example_r, der_example_r, h);

    matlab_dump(domain.positions(), "starting_pts.m", "pts");

    //----------------------------------//
    // Now we try to insert more points //
    //----------------------------------//

    // Define a new DomainDiscretization
    UnknownShape<Vec2d> fill_shape;
    DomainDiscretization<Vec2d> filled_domain(fill_shape);

    // Initialize an EdgeFiller2D object with parameters
    //  - points ... a Range<Vec2d> or DomainDiscretization<Vec2d> containg the existing points
    //  - seed ... an int defining the rng seed for gsf; default is 1
    //  - zeta ... a double regulating proximity tolerance in gsf; default is 1 - 1e-10
    EdgeFiller2D ef(domain);

    // Fill the new DomainDiscretization. The input arguments are
    //  - domain ... a DomainDiscretization to be filled
    //  - h ... a function that determines the desired space between neighboring new points;
    //          can also be given as a double if the function is constant
    ef(filled_domain, h/10);

    matlab_dump(filled_domain.positions(), "filled_pts.m", "fill");
}

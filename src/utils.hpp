#ifndef SPLINE_FILLER_UTILS_HPP
#define SPLINE_FILLER_UTILS_HPP

#include <iostream>
#include <fstream>
#include <medusa/bits/types/Range_fwd.hpp>
using namespace mm;

template <class T>
void matlab_dump(const Range<T>& range, const std::string& filename, const std::string& var_name) {
    const std::string dir = "../data/";
    std::string path = dir + filename;
    std::ofstream out_file(path);
    out_file << var_name << " = " << range << ";" << std::endl;
    out_file.close();
}

#endif //SPLINE_FILLER_UTILS_HPP

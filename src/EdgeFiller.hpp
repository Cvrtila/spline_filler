#ifndef SPLINE_FILLER_EDGEFILLER_HPP
#define SPLINE_FILLER_EDGEFILLER_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>

using namespace mm;

class EdgeFiller2D {
public:
    int seed;
    int nr_of_points;
    Range<Vec2d> points;
    Range<int> order_permutation;
    double h;
    Range<double> knots;
    Eigen::VectorXd Mx, My;
    double zeta;

    EdgeFiller2D(const Range<Vec2d> &points, int seed=1, double zeta = 1 - 1e-10) {
        init(points, seed, zeta);
    }

    EdgeFiller2D(const DomainDiscretization<Vec2d> &points, int seed=1, double zeta = 1 - 1e-10) {
        init(points.positions(), seed, zeta);
    }

    template<typename spacing_t>
    void operator () (DomainDiscretization<Vec2d> &domain, spacing_t h) {
        // Now call GSF on each of the polynomials that make up the spline;
        // however we first want to add the endpoints as seeds for GSF
        GeneralSurfaceFill<Vec2d, Vec1d> gsf;
        gsf.seed(seed).proximityTolerance(zeta);
        KDTreeMutable<Vec2d> tree;
        for (int k = 0; k < nr_of_points; ++k) {
            // This is the parametrisation that we will be using in GSF
            auto r = [this, &k](Vec1d v) {return this->evalPart(v(0), k);};
            auto dr = [this, &k](Vec1d v) {return this->evalJacobi(v(0), k);};
            BoxShape<Vec1d> shape = getPartDomain(k);

            // Now we add the endpoints
            DomainDiscretization<Vec1d> param_dom(shape);
            Range<Vec1d> param_seeds{shape.beg(), shape.end()};
            for (const Vec1d& param_seed : param_seeds) {
                param_dom.addInternalNode(param_seed, 1);
                // Check for insertion into domain.
                Vec2d pt_seed = r(param_seed);
                double check_radius_seed = h(pt_seed);
                double d_sq = tree.size() == 0 ? 10 * check_radius_seed * check_radius_seed : tree.query(pt_seed).second[0];
                if (d_sq >= (zeta * check_radius_seed) * (zeta * check_radius_seed)) {
                    Vec2d normal_seed = surface_fill_internal::compute_normal(dr(param_seed));
                    domain.addBoundaryNode(pt_seed, -1, normal_seed);
                    tree.insert(pt_seed);
                }
            }

            // Finally we call gsf
            gsf(domain, param_dom, r, dr, h, tree);
        }
    }

    void operator () (DomainDiscretization<Vec2d>& domain, double h) {
        auto func_h = [&h](Vec2d t){
            return h;
        };
        this->operator()(domain, func_h);
    }

private:
    void init(const Range<Vec2d> &pts, int s, double z) {
        points = pts;
        nr_of_points = points.size();
        seed = s;
        zeta = z;

        order_permutation = Range<int>(nr_of_points);
        order_permutation[0] = 0;
        KDTree<Vec2d> edge_kdtree(points);
        Range<double> distances_square;
        Range<int> indices;

        // Order points so that the point indexed by order_permutation[i+1] is a neighbor of order_permutation[i]
        for (int i = 0; i+1 < nr_of_points; ++i) {
            // Find the (i+1)-th index
            // Start by looking at the nn = 2 knn, if none are applicable, increase nn by 1
            int nn = 1;
            bool looking_for_nbr = true;
            int candidate;
            while (looking_for_nbr) {
                nn++; // Increase nn by 1 - every time we don't find the right index
                // Find the nn knn of the current point, check each if it wasn't already enumerated
                std::tie(indices, distances_square) = edge_kdtree.query(points[order_permutation[i]], nn);
                // Look at the latest candidate - the last one returned by query - since we checked all others
                // and the 0th one is just the current point
                candidate = nn - 1;
                bool candidate_ok = true;
                // Compare the index of the current candidate to the previous nn points
                // If it was already enumerated, skip
                for (int prev = 1; prev < nn; prev++) {
                    candidate_ok = (indices[candidate]==order_permutation[std::max(0, i-prev)])? false : candidate_ok;
                }
                // When we eventually find the next neighbor, set it as the next point
                if (candidate_ok) {
                    order_permutation[i+1] = indices[candidate];
                    looking_for_nbr = false;
                }
            }
        }
        // From now on we only access pts by first reindexing through order_permutation

        // Now we interpolate a spline through these points
        // The knots will be evenly placed
        knots = Range<double>::seq(nr_of_points + 1);
        // First we construct the interpolation matrix
        // This could be sped up by just implementing Thomas' algorithm,
        // however, we would still need to construct the RHS vectors
        // If there are n points, the interpolation matrix will be tridiagonal (n+1)x(n+1),
        // as if we were adding a copy of pts[order_permutation[0]] to the end of pts
        Eigen::SparseMatrix<double> interpolation_matrix(nr_of_points+1, nr_of_points+1);
        Eigen::VectorXd dx(nr_of_points+1);
        Eigen::VectorXd dy(nr_of_points+1);

        // Since the knots are evenly spaced, the interpolation matrix will be simple
        double mi0 = 0.5;
        double lambda0 = 0.5;
        // By the same reasoning, knots[0] == 0 and knots[i] == i*knots[1]
        h = knots[1];
        Range<Eigen::Triplet<double>> elems;
        for (int i = 0; i <= nr_of_points - 1; ++i) {
            elems.push_back(Eigen::Triplet<double>(i, i, 2));
        }
        for (int i = 1; i < nr_of_points-1; ++i) {
            elems.push_back(Eigen::Triplet<double>(i, i+1, lambda0));
        }
        for (int i = 0; i < nr_of_points - 1 -1; ++i) {
            elems.push_back(Eigen::Triplet<double>(i+1, i, mi0));
        }
        elems.push_back(Eigen::Triplet<double>(nr_of_points, 0, 1));
        elems.push_back(Eigen::Triplet<double>(nr_of_points, 1, 0.5));
        elems.push_back(Eigen::Triplet<double>(nr_of_points, nr_of_points-1, 0.5));
        elems.push_back(Eigen::Triplet<double>(nr_of_points, nr_of_points, 1));
        interpolation_matrix.setFromTriplets(elems.begin(), elems.end());

        // Now we construct the RHS vector
        for (int i = 1; i < nr_of_points-1; ++i) {
            dx(i) = 6*dd3(knots[i-1], knots[i], knots[i+1], points[order_permutation[i-1]](0), points[order_permutation[i]](0), points[order_permutation[i+1]](0));
            dy(i) = 6*dd3(knots[i-1], knots[i], knots[i+1], points[order_permutation[i-1]](1), points[order_permutation[i]](1), points[order_permutation[i+1]](1));
        }
        dx(nr_of_points) = 6*dd3(knots[nr_of_points-2], knots[nr_of_points-1], knots[nr_of_points], points[order_permutation[nr_of_points-1]](0), points[order_permutation[0]](0), points[order_permutation[1]](0));
        dy(nr_of_points) = 6*dd3(knots[nr_of_points-2], knots[nr_of_points-1], knots[nr_of_points], points[order_permutation[nr_of_points-1]](1), points[order_permutation[0]](1), points[order_permutation[1]](1));

        // Now we finally get the coefficients of the polynomials that make up the spline by solving the system
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double>> solver;
        solver.compute(interpolation_matrix);

        Mx = solver.solve(dx);
        My = solver.solve(dy);
    }

    BoxShape<Vec1d> getPartDomain(int k) {
        return BoxShape<Vec1d>(Vec1d(knots[k]), Vec1d(knots[k+1]));
    }

    Vec2d evalPart(double t, int k) {
        double xp, yp;
        xp = Mx(k) * std::pow(knots[k + 1] - t, 3) / (6 * h) +
             Mx(k + 1) * std::pow(t - knots[k], 3) / (6 * h) +
             (points[order_permutation[k % nr_of_points]](0) - Mx(k) * std::pow(h, 2) / 6) * (knots[k + 1] - t) / h +
             (points[order_permutation[(k + 1) % nr_of_points]](0) - Mx(k + 1) * std::pow(h, 2) / 6) * (t - knots[k]) / h;
        yp = My(k) * std::pow(knots[k + 1] - t, 3) / (6 * h) +
             My(k + 1) * std::pow(t - knots[k], 3) / (6 * h) +
             (points[order_permutation[k % nr_of_points]](1) - My(k) * std::pow(h, 2) / 6) * (knots[k + 1] - t) / h +
             (points[order_permutation[(k + 1) % nr_of_points]](1) - My(k + 1) * std::pow(h, 2) / 6) * (t - knots[k]) / h;
        return Vec2d(xp, yp);
    }

    Vec2d evalJacobi(double t, int k) {
        double xp, yp;
        xp = -Mx(k)*std::pow(knots[k+1] - t, 2)/(2*h) +
             Mx(k+1)*std::pow(t - knots[k], 2)/(2*h) -
             (points[order_permutation[k % nr_of_points]](0) - Mx(k)*std::pow(h, 2)/6)/h +
             (points[order_permutation[(k+1) % nr_of_points]](0) - Mx(k+1)*std::pow(h, 2)/6)/h;
        yp = -My(k)*std::pow(knots[k+1] - t, 2)/(2*h) +
             My(k+1)*std::pow(t - knots[k], 2)/(2*h) -
             (points[order_permutation[k % nr_of_points]](1) - My(k)*std::pow(h, 2)/6)/h +
             (points[order_permutation[(k+1) % nr_of_points]](1) - My(k+1)*std::pow(h, 2)/6)/h;
        return Vec2d(xp, yp);
    }

    double dd3(double x0, double x1, double x2, double y0, double y1, double y2) {
        // Calculates divided differences on three points, i.e. if y_i = f(x_i), then dd3 calculates
        // the value of [x_0, x_1, x_2]f
        return ((y2 - y1)/(x2 - x1) - (y1 - y0)/(x1 - x0))/(x2 - x0);
    }
};


#endif //SPLINE_FILLER_EDGEFILLER_HPP

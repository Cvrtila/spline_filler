clear;

run ../data/starting_pts.m
run ../data/filled_pts.m

scatter(pts(1, :), pts(2, :));
axis equal
hold on;
scatter(fill(1, :), fill(2, :), '.');
scatter(pts(1, 1), pts(2, 1), 'g', 'filled');
hold off;
